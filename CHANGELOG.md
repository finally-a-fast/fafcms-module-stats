[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Module statistics
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Link tracking @cmoeke
- Page action id to js url param @cmoeke
- Time to js url param @cmoeke

### Changed
- Added return type to prefixableTableName, rules and attributeLabels @cmoeke
- Regenerated models @cmoeke
- Changed transparent image to a real file @cmoeke
- Changed saved time of page view to client time if it not differs too much from server time @cmoeke
- Disabled change log for statistic models @cmoeke
- Column sizes for fomantic @cmoeke

### Fixed
- Fixed useragent length @cmoeke 
- Static function editDataSingular @cmoeke
  
## [0.1.0] - 2020-09-29
### Added
- CHANGELOG.md @cmoeke fafcms-core#39
- basic doc folder structure @cmoeke fafcms-core/37
- .gitlab-ci.yml for doc an build creation @cmoeke fafcms-core/38

### Changed
- composer.json to use correct dependencies @cmoeke
- LICENSE to LICENSE.md @cmoeke
- PHP dependency to 7.4 @cmoeke
- Changed ci config to handle new build branch @cmoeke
- Replaced moment js with luxon @cmoeke

### Fixed
- Broken README.md icons @cmoeke fafcms-core#46

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-module-stats/-/tree/master
[0.1.0]: https://gitlab.com/finally-a-fast/fafcms-module-stats/-/tree/v0.1.0-alpha
