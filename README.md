[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Readme | Module statistics
================================================

[![Latest Stable Version](https://img.shields.io/packagist/v/finally-a-fast/fafcms-module-stats?label=stable&style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-module-stats)
[![Latest Version](https://img.shields.io/packagist/v/finally-a-fast/fafcms-module-stats?include_prereleases&label=unstable&style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-module-stats)
[![PHP Version](https://img.shields.io/packagist/php-v/finally-a-fast/fafcms-module-stats/dev-master?style=flat-square)](https://www.php.net/downloads.php)
[![License](https://img.shields.io/packagist/l/finally-a-fast/fafcms-module-stats?style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-module-stats)
[![Total Downloads](https://img.shields.io/packagist/dt/finally-a-fast/fafcms-module-stats?style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-module-stats)
[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat-square)](http://www.yiiframework.com/)


This is an module for the Finally a fast CMS.

Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run
```
php composer.phar require finally-a-fast/fafcms-module-stats
```
or add
```
"finally-a-fast/fafcms-module-stats": "dev-master"
```
to the require section of your `composer.json` file.

Documentation
------------

[Documentation](https://www.finally-a-fast.com/) will be added soon at https://www.finally-a-fast.com/.

License
-------

**fafcms-module-stats** is released under the MIT License. See the [LICENSE.md](LICENSE.md) for details.
