<?php

namespace fafcms\stats;

use DeviceDetector\DeviceDetector;
use Exception;
use fafcms\fafcms\components\FafcmsComponent;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\helpers\interfaces\ContentmetaInterface;
use fafcms\stats\assets\PageActionAsset;
use fafcms\stats\models\Layout;
use fafcms\stats\models\Menu;
use fafcms\stats\models\Site;
use fafcms\stats\models\Snippet;
use fafcms\stats\models\Topic;
use fafcms\stats\models\Useragent;
use fafcms\stats\models\View as ViewModel;
use fafcms\stats\models\Viewsession;
use yii\base\Application;
use Yii;
use yii\base\BootstrapInterface;
use yii\db\Expression;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\i18n\PhpMessageSource;
use yii\log\Logger;
use yii\base\Event;
use yii\web\User;
use fafcms\fafcms\components\ViewComponent;

class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-stats';
    public static $tablePrefix = 'fafcms-stats_';

    public static $backendUrlRules = [];

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-stats'])) {
            $app->i18n->translations['fafcms-stats'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    /**
     * @param Application $app
     * @param PluginModule $module
     * @return bool
     */
    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        /*$app->fafcms->addSidebarItems('analyses', [
            'icon' => 'chart-line',
            'label' => Yii::t('fafcms-stats', 'Analyses'),
        ], 'project-group');*/

        $app->fafcms->addFrontendUrlRules(self::$id, [
            'page-action' => 'basic-tracking/api',
        ]);

        $app->fafcms->addBackendUrlRules(self::$id, [
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>/<id:[a-zA-Z0-9\-\_]+>' => '<controller>/<action>',
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>' => '<controller>/<action>',
        ]);

        $app->view->addViewItems($module->defaultEditViewItems, 'model\\edit\\', null, ContentmetaInterface::class);
        $app->view->addViewItems($module->defaultDashboardViewItems, 'fafcms\\ProjectDashboard');

        $basicTrackingViewsessionId = null;

        Event::on(User::class, User::EVENT_BEFORE_LOGOUT, static function () use (&$basicTrackingViewsessionId) {
            $basicTrackingViewsessionId = Yii::$app->getSession()->get('basic-tracking-viewsession-id');
        });

        Event::on(User::class, User::EVENT_AFTER_LOGOUT, static function () use (&$basicTrackingViewsessionId) {
            Yii::$app->getSession()->set('basic-tracking-viewsession-id', $basicTrackingViewsessionId);
        });

        return true;
    }

    protected function bootstrapFrontendApp(Application $app, PluginModule $module): bool
    {
        try {
            if (!Yii::$app->request->isAjax && ($module->getPluginSettingValue('enable_basic_tracking') || $module->getPluginSettingValue('enable_link_tracking'))) {
                Event::on(ViewComponent::class, ViewComponent::EVENT_END_BODY, static function () use ($module) {
                    $pageview = null;

                    if ($module->getPluginSettingValue('enable_basic_tracking')) {
                        try {
                            $userAgentString = $_SERVER['HTTP_USER_AGENT'] ?? '';

                            $userAgent = Yii::$app->cache->getOrSet('stat_useragent_' . md5($userAgentString), static function () use ($userAgentString) {
                                $userAgent = Useragent::find()->where(['agent' => $userAgentString])->one();

                                if ($userAgent === null) {
                                    $deviceDetector = new DeviceDetector($userAgentString);

                                    //$deviceDetector->setCache(new PSR6Bridge(new Psr6ToYii2Cache(Yii::$app->cache)));
                                    $deviceDetector->discardBotInformation();
                                    $deviceDetector->parse();

                                    if ($deviceDetector->isBot()) {
                                        return -1;
                                    }

                                    $device = $deviceDetector->getDeviceName();
                                    $brand = $deviceDetector->getBrandName();
                                    $model = $deviceDetector->getModel();
                                    $osInfo = $deviceDetector->getOs();
                                    $clientInfo = $deviceDetector->getClient();

                                    $userAgent = new Useragent();
                                    $userAgent->loadDefaultValues();
                                    $userAgent->setAttributes([
                                        'agent'           => $userAgentString,
                                        'device_name'     => $device,
                                        'brand_name'      => $brand,
                                        'model'           => $model,
                                        'os_name'         => $osInfo['name'] ?? '',
                                        'os_version'      => $osInfo['version'] ?? '',
                                        'browser_name'    => $clientInfo['name'] ?? '',
                                        'browser_version' => $clientInfo['version'] ?? '',
                                    ]);

                                    if (!$userAgent->save()) {
                                        Yii::$app->getLog()->getLogger()->log('Cannot create Useragent: '.print_r($userAgent->getErrors(), true), Logger::LEVEL_ERROR);
                                        return false;
                                    }
                                }

                                return $userAgent->id;
                            }, null);

                            if ($userAgent !== false && $userAgent !== -1) {
                                $now = (new \DateTime('NOW', new \DateTimeZone(Yii::$app->formatter->defaultTimeZone)))->format('Y-m-d H:i:s');

                                if (($viewSessionId = Yii::$app->getSession()->get('basic-tracking-viewsession-id')) === null || ($viewSession = Viewsession::find()->where(['hashId' => $viewSessionId])->one()) === null) {
                                    $viewSession = new Viewsession();
                                    $viewSession->loadDefaultValues();
                                    $viewSession->setAttributes([
                                        'enter_at'      => $now,
                                        'last_focus_at' => $now,
                                        'useragent_id'  => $userAgent,
                                    ]);

                                    if ($viewSession->save()) {
                                        Yii::$app->getSession()->set('basic-tracking-viewsession-id', $viewSession->getHashId());
                                    } else {
                                        Yii::$app->getLog()->getLogger()->log('Cannot create Viewsession: '.print_r($viewSession->getErrors(), true), Logger::LEVEL_ERROR);
                                        $viewSession = null;
                                    }
                                }

                                if ($viewSession !== null) {
                                    $pageview = new ViewModel();
                                    $pageview->loadDefaultValues();
                                    $pageview->setAttributes([
                                        'statuscode'     => Yii::$app->response->getStatusCode(),
                                        'url'            => Yii::$app->request->url,
                                        'referrer'       => str_replace(
                                            [
                                                'https://' . Yii::$app->fafcms->getCurrentProjectLanguage()->domain->domain,
                                                'http://' . Yii::$app->fafcms->getCurrentProjectLanguage()->domain->domain,
                                            ],
                                            '',
                                            Yii::$app->request->referrer
                                        ),
                                        'viewsession_id' => $viewSession->id,
                                        'user_id'        => (Yii::$app->user->isGuest ? null : Yii::$app->user->id),
                                        'usergroup'      => (Yii::$app->user->isGuest ? null : Yii::$app->user->identity->getUserGroupLabel()),
                                        'enter_at'       => $now,
                                        'last_focus_at'  => $now,
                                    ]);

                                    if (!$pageview->save()) {
                                        Yii::$app->getLog()->getLogger()->log('Cannot create View: '.print_r($pageview->getErrors(), true), Logger::LEVEL_ERROR);
                                        $pageview = null;
                                    }
                                }
                            }
                        } catch (Exception $e) {
                            var_dump($e->getMessage());die();
                            Yii::$app->getLog()->getLogger()->log('Cannot save basic tracking data. Error: ' . $e->getMessage(), Logger::LEVEL_ERROR);
                        }
                    }

                    if ($module->getPluginSettingValue('enable_link_tracking')) {

                    }

                    $trackingUrl = Url::to(['/' . self::$id . '/basic-tracking/api']);

                    $pageActionConfig = [
                        'url'          => $trackingUrl,
                        'basic'        => $module->getPluginSettingValue('enable_basic_tracking') === 1,
                        'link'         => $module->getPluginSettingValue('enable_link_tracking') === 1,
                        'linkSelector' => $module->getPluginSettingValue('link_tracking_selector'),
                    ];

                    if ($pageview !== null) {
                        $pageActionConfig['id'] = $pageview->getHashId();
                    }

                    Yii::$app->fafcms->setJsConfigValue('page-action', $pageActionConfig);

                    PageActionAsset::register(Yii::$app->view);

                    return true;
                });
            }
        } catch (Exception $e) {
            var_dump($e->getMessage());die();
            Yii::$app->getLog()->getLogger()->log('Cannot save tracking data. Error: ' . $e->getMessage(), Logger::LEVEL_ERROR);
        }

        return true;
    }
}
