<?php

namespace fafcms\stats;

use fafcms\fafcms\inputs\TextInput;
use fafcms\stats\charts\BrowserUsage;
use fafcms\stats\charts\PageViews;
use Yii;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\stats\models\Contentmeta;
use fafcms\helpers\classes\PluginSetting;
use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\inputs\Checkbox;

class Module extends PluginModule
{
    public array $defaultEditViewItems = [
        'stats-tab' => [
            'class' => \fafcms\fafcms\items\Tab::class,
            'settings' => [
                'label' => ['fafcms-stats', 'Statistics'],
            ],
            'contents' => [
                'row-1' => [
                    'class' => \fafcms\fafcms\items\Row::class,
                    'contents' => [
                        'page-properties-column' => [
                            'class' => \fafcms\fafcms\items\Column::class,
                            'settings' => [
                                'm' => 8,
                            ],
                            'contents' => [
                                'stats-views-card' => [
                                    'class' => \fafcms\fafcms\items\Card::class,
                                    'settings' => [
                                        'title' => ['fafcms-stats', 'Page views'],
                                        'icon' => 'chart-line',
                                    ],
                                    'contents' => [
                                        [
                                            'class' => \fafcms\stats\items\Chart::class,
                                        ],
                                    ]
                                ],
                            ]
                        ],
                    ],
                ],
            ]
        ]
    ];

    public array $defaultDashboardViewItems = [
        'home-tab' => [
            'class' => \fafcms\fafcms\items\Tab::class,
            'settings' => [
                'label' => ['fafcms-core', 'Home'],
            ],
            'contents' => [
                'row-stats' => [
                    'class' => \fafcms\fafcms\items\Row::class,
                    'contents' => [
                        'column-1' => [
                            'class' => \fafcms\fafcms\items\Column::class,
                            'settings' => [
                                'm' => 8,
                            ],
                            'contents' => [
                                'stats-browser-card' => [
                                    'class' => \fafcms\fafcms\items\Card::class,
                                    'settings' => [
                                        'title' => ['fafcms-stats', 'Browser usage'],
                                        'icon' => 'chart-donut',
                                    ],
                                    'contents' => [
                                        [
                                            'class' => \fafcms\stats\items\Chart::class,
                                            'settings' => [
                                                'type' => 'pie',
                                                'datasource' => BrowserUsage::class
                                            ],
                                        ],
                                    ]
                                ],
                            ]
                        ],
                        'column-2' => [
                            'class' => \fafcms\fafcms\items\Column::class,
                            'settings' => [
                                'm' => 8,
                            ],
                            'contents' => [
                                'stats-views-card' => [
                                    'class' => \fafcms\fafcms\items\Card::class,
                                    'settings' => [
                                        'title' => ['fafcms-stats', 'Page views'],
                                        'icon' => 'chart-line',
                                    ],
                                    'contents' => [
                                        [
                                            'class' => \fafcms\stats\items\Chart::class,
                                            'settings' => [
                                                'datasource' => PageViews::class
                                            ],
                                        ],
                                    ]
                                ],
                            ]
                        ],
                    ],
                ],
            ]
        ]
    ];

    public function getPluginSettingRules(): array
    {
        return [
            [['enable_basic_tracking'], 'boolean'],
            [['enable_link_tracking'], 'boolean'],
            [['link_tracking_selector'], 'string']
        ];
    }

    protected function pluginSettingDefinitions(): array
    {
        return [
            new PluginSetting($this, [
                'name' => 'enable_basic_tracking',
                'label' => Yii::t('fafcms-stats', 'Enable basic tracking'),
                'defaultValue' => false,
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
            ]),
            new PluginSetting($this, [
                'name' => 'enable_link_tracking',
                'label' => Yii::t('fafcms-stats', 'Enable link tracking'),
                'description' => Yii::t('fafcms-stats', 'Only links with the link tracking class gets tracked.'),
                'defaultValue' => true,
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
            ]),
            new PluginSetting($this, [
                'name' => 'link_tracking_selector',
                'label' => Yii::t('fafcms-stats', 'Link tracking selector'),
                'description' => Yii::t('fafcms-stats', 'Only links which match the specified selector gets tracked.'),
                'defaultValue' => '.fafcms-page-action-link',
                'inputType' => TextInput::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
            ]),
        ];
    }
}
