<?php

namespace fafcms\stats\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\TextInput,
    inputs\UrlInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\User,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\stats\{
    Bootstrap,
    models\View,
    models\Viewsession,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%link}}".
 *
 * @package fafcms\stats\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string|null $url
 * @property string|null $referrer
 * @property int|null $view_id
 * @property int|null $viewsession_id
 * @property int|null $user_id
 * @property string|null $usergroup
 * @property string|null $click_at
 *
 * @property User $user
 * @property View $view
 * @property Viewsession $viewsession
 */
abstract class BaseLink extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/link';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'link';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-stats', 'Links');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-stats', 'Link');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'view_id' => static function($properties = []) {
                return View::getOptionProvider($properties)->getOptions();
            },
            'viewsession_id' => static function($properties = []) {
                return Viewsession::getOptionProvider($properties)->getOptions();
            },
            'user_id' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'url' => [
                'type' => UrlInput::class,
            ],
            'referrer' => [
                'type' => TextInput::class,
            ],
            'view_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('view_id', false),
                'relationClassName' => View::class,
            ],
            'viewsession_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('viewsession_id', false),
                'relationClassName' => Viewsession::class,
            ],
            'user_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('user_id', false),
                'relationClassName' => User::class,
            ],
            'usergroup' => [
                'type' => TextInput::class,
            ],
            'click_at' => [
                'type' => DateTimePicker::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'url' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'url',
                        'sort' => 2,
                    ],
                ],
                'referrer' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'referrer',
                        'sort' => 3,
                    ],
                ],
                'view_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'view_id',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                'viewsession_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'viewsession_id',
                        'sort' => 5,
                        'link' => true,
                    ],
                ],
                'user_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'user_id',
                        'sort' => 6,
                        'link' => true,
                    ],
                ],
                'usergroup' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'usergroup',
                        'sort' => 7,
                    ],
                ],
                'click_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'click_at',
                        'sort' => 8,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-url' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'url',
                                                    ],
                                                ],
                                                'field-referrer' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'referrer',
                                                    ],
                                                ],
                                                'field-view_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'view_id',
                                                    ],
                                                ],
                                                'field-viewsession_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'viewsession_id',
                                                    ],
                                                ],
                                                'field-user_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'user_id',
                                                    ],
                                                ],
                                                'field-usergroup' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'usergroup',
                                                    ],
                                                ],
                                                'field-click_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'click_at',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%link}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'integer-view_id' => ['view_id', 'integer'],
            'integer-viewsession_id' => ['viewsession_id', 'integer'],
            'integer-user_id' => ['user_id', 'integer'],
            'date-click_at' => ['click_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'click_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-url' => ['url', 'string', 'max' => 5000],
            'string-referrer' => ['referrer', 'string', 'max' => 5000],
            'string-usergroup' => ['usergroup', 'string', 'max' => 255],
            'exist-user_id' => [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            'exist-view_id' => [['view_id'], 'exist', 'skipOnError' => true, 'targetClass' => View::class, 'targetAttribute' => ['view_id' => 'id']],
            'exist-viewsession_id' => [['viewsession_id'], 'exist', 'skipOnError' => true, 'targetClass' => Viewsession::class, 'targetAttribute' => ['viewsession_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-stats', 'ID'),
            'url' => Yii::t('fafcms-stats', 'Url'),
            'referrer' => Yii::t('fafcms-stats', 'Referrer'),
            'view_id' => Yii::t('fafcms-stats', 'View ID'),
            'viewsession_id' => Yii::t('fafcms-stats', 'Viewsession ID'),
            'user_id' => Yii::t('fafcms-stats', 'User ID'),
            'usergroup' => Yii::t('fafcms-stats', 'Usergroup'),
            'click_at' => Yii::t('fafcms-stats', 'Click At'),
        ]);
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'user_id',
        ]);
    }

    /**
     * Gets query for [[View]].
     *
     * @return ActiveQuery
     */
    public function getView(): ActiveQuery
    {
        return $this->hasOne(View::class, [
            'id' => 'view_id',
        ]);
    }

    /**
     * Gets query for [[Viewsession]].
     *
     * @return ActiveQuery
     */
    public function getViewsession(): ActiveQuery
    {
        return $this->hasOne(Viewsession::class, [
            'id' => 'viewsession_id',
        ]);
    }
}
