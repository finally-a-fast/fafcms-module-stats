<?php

namespace fafcms\stats\abstracts\models;

use fafcms\fafcms\{
    inputs\NumberInput,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\stats\{
    Bootstrap,
    models\Viewsummary,
};
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the abstract model class for table "{{%useragent}}".
 *
 * @package fafcms\stats\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string|null $agent
 * @property string|null $device_name
 * @property string|null $brand_name
 * @property string|null $model
 * @property string|null $os_name
 * @property string|null $os_version
 * @property string|null $browser_name
 * @property string|null $browser_version
 *
 * @property Viewsummary[] $useragentViewsummaries
 */
abstract class BaseUseragent extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/useragent';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'useragent';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-stats', 'Useragents');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-stats', 'Useragent');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'agent' => [
                'type' => TextInput::class,
            ],
            'device_name' => [
                'type' => TextInput::class,
            ],
            'brand_name' => [
                'type' => TextInput::class,
            ],
            'model' => [
                'type' => TextInput::class,
            ],
            'os_name' => [
                'type' => TextInput::class,
            ],
            'os_version' => [
                'type' => TextInput::class,
            ],
            'browser_name' => [
                'type' => TextInput::class,
            ],
            'browser_version' => [
                'type' => TextInput::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'agent' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'agent',
                        'sort' => 2,
                    ],
                ],
                'device_name' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'device_name',
                        'sort' => 3,
                    ],
                ],
                'brand_name' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'brand_name',
                        'sort' => 4,
                    ],
                ],
                'model' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'model',
                        'sort' => 5,
                    ],
                ],
                'os_name' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'os_name',
                        'sort' => 6,
                    ],
                ],
                'os_version' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'os_version',
                        'sort' => 7,
                    ],
                ],
                'browser_name' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'browser_name',
                        'sort' => 8,
                    ],
                ],
                'browser_version' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'browser_version',
                        'sort' => 9,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-agent' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'agent',
                                                    ],
                                                ],
                                                'field-device_name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'device_name',
                                                    ],
                                                ],
                                                'field-brand_name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'brand_name',
                                                    ],
                                                ],
                                                'field-model' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'model',
                                                    ],
                                                ],
                                                'field-os_name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'os_name',
                                                    ],
                                                ],
                                                'field-os_version' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'os_version',
                                                    ],
                                                ],
                                                'field-browser_name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'browser_name',
                                                    ],
                                                ],
                                                'field-browser_version' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'browser_version',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%useragent}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'string-agent' => ['agent', 'string', 'max' => 5000],
            'string-device_name' => ['device_name', 'string', 'max' => 255],
            'string-brand_name' => ['brand_name', 'string', 'max' => 255],
            'string-model' => ['model', 'string', 'max' => 255],
            'string-os_name' => ['os_name', 'string', 'max' => 255],
            'string-os_version' => ['os_version', 'string', 'max' => 255],
            'string-browser_name' => ['browser_name', 'string', 'max' => 255],
            'string-browser_version' => ['browser_version', 'string', 'max' => 255],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-stats', 'ID'),
            'agent' => Yii::t('fafcms-stats', 'Agent'),
            'device_name' => Yii::t('fafcms-stats', 'Device Name'),
            'brand_name' => Yii::t('fafcms-stats', 'Brand Name'),
            'model' => Yii::t('fafcms-stats', 'Model'),
            'os_name' => Yii::t('fafcms-stats', 'Os Name'),
            'os_version' => Yii::t('fafcms-stats', 'Os Version'),
            'browser_name' => Yii::t('fafcms-stats', 'Browser Name'),
            'browser_version' => Yii::t('fafcms-stats', 'Browser Version'),
        ]);
    }

    /**
     * Gets query for [[UseragentViewsummaries]].
     *
     * @return ActiveQuery
     */
    public function getUseragentViewsummaries(): ActiveQuery
    {
        return $this->hasMany(Viewsummary::class, [
            'useragent_id' => 'id',
        ]);
    }
}
