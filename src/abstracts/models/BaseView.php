<?php

namespace fafcms\stats\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\TextInput,
    inputs\UrlInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\stats\{
    Bootstrap,
    models\Link,
    models\Viewsession,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%view}}".
 *
 * @package fafcms\stats\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int $statuscode
 * @property string|null $url
 * @property string|null $referrer
 * @property int $viewsession_id
 * @property int|null $user_id
 * @property string|null $usergroup
 * @property string|null $enter_at
 * @property string|null $last_focus_at
 * @property string|null $last_blur_at
 * @property string|null $leave_at
 * @property int $duration
 *
 * @property Link[] $viewLinks
 * @property Viewsession $viewsession
 */
abstract class BaseView extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/view';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'view';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-stats', 'Views');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-stats', 'View');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'viewsession_id' => static function($properties = []) {
                return Viewsession::getOptionProvider($properties)->getOptions();
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'statuscode' => [
                'type' => NumberInput::class,
            ],
            'url' => [
                'type' => UrlInput::class,
            ],
            'referrer' => [
                'type' => TextInput::class,
            ],
            'viewsession_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('viewsession_id', false),
                'relationClassName' => Viewsession::class,
            ],
            'user_id' => [
                'type' => NumberInput::class,
            ],
            'usergroup' => [
                'type' => TextInput::class,
            ],
            'enter_at' => [
                'type' => DateTimePicker::class,
            ],
            'last_focus_at' => [
                'type' => DateTimePicker::class,
            ],
            'last_blur_at' => [
                'type' => DateTimePicker::class,
            ],
            'leave_at' => [
                'type' => DateTimePicker::class,
            ],
            'duration' => [
                'type' => NumberInput::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'statuscode' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'statuscode',
                        'sort' => 2,
                    ],
                ],
                'url' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'url',
                        'sort' => 3,
                    ],
                ],
                'referrer' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'referrer',
                        'sort' => 4,
                    ],
                ],
                'viewsession_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'viewsession_id',
                        'sort' => 5,
                        'link' => true,
                    ],
                ],
                'user_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'user_id',
                        'sort' => 6,
                    ],
                ],
                'usergroup' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'usergroup',
                        'sort' => 7,
                    ],
                ],
                'enter_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'enter_at',
                        'sort' => 8,
                    ],
                ],
                'last_focus_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_focus_at',
                        'sort' => 9,
                    ],
                ],
                'last_blur_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_blur_at',
                        'sort' => 10,
                    ],
                ],
                'leave_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'leave_at',
                        'sort' => 11,
                    ],
                ],
                'duration' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'duration',
                        'sort' => 12,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-statuscode' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'statuscode',
                                                    ],
                                                ],
                                                'field-url' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'url',
                                                    ],
                                                ],
                                                'field-referrer' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'referrer',
                                                    ],
                                                ],
                                                'field-viewsession_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'viewsession_id',
                                                    ],
                                                ],
                                                'field-user_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'user_id',
                                                    ],
                                                ],
                                                'field-usergroup' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'usergroup',
                                                    ],
                                                ],
                                                'field-enter_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'enter_at',
                                                    ],
                                                ],
                                                'field-last_focus_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_focus_at',
                                                    ],
                                                ],
                                                'field-last_blur_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_blur_at',
                                                    ],
                                                ],
                                                'field-leave_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'leave_at',
                                                    ],
                                                ],
                                                'field-duration' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'duration',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%view}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'integer-statuscode' => ['statuscode', 'integer'],
            'integer-viewsession_id' => ['viewsession_id', 'integer'],
            'integer-user_id' => ['user_id', 'integer'],
            'integer-duration' => ['duration', 'integer'],
            'required-viewsession_id' => ['viewsession_id', 'required'],
            'date-enter_at' => ['enter_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'enter_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-last_focus_at' => ['last_focus_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'last_focus_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-last_blur_at' => ['last_blur_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'last_blur_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-leave_at' => ['leave_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'leave_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-url' => ['url', 'string', 'max' => 5000],
            'string-referrer' => ['referrer', 'string', 'max' => 5000],
            'string-usergroup' => ['usergroup', 'string', 'max' => 255],
            'exist-viewsession_id' => [['viewsession_id'], 'exist', 'skipOnError' => true, 'targetClass' => Viewsession::class, 'targetAttribute' => ['viewsession_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-stats', 'ID'),
            'statuscode' => Yii::t('fafcms-stats', 'Statuscode'),
            'url' => Yii::t('fafcms-stats', 'Url'),
            'referrer' => Yii::t('fafcms-stats', 'Referrer'),
            'viewsession_id' => Yii::t('fafcms-stats', 'Viewsession ID'),
            'user_id' => Yii::t('fafcms-stats', 'User ID'),
            'usergroup' => Yii::t('fafcms-stats', 'Usergroup'),
            'enter_at' => Yii::t('fafcms-stats', 'Enter At'),
            'last_focus_at' => Yii::t('fafcms-stats', 'Last Focus At'),
            'last_blur_at' => Yii::t('fafcms-stats', 'Last Blur At'),
            'leave_at' => Yii::t('fafcms-stats', 'Leave At'),
            'duration' => Yii::t('fafcms-stats', 'Duration'),
        ]);
    }

    /**
     * Gets query for [[ViewLinks]].
     *
     * @return ActiveQuery
     */
    public function getViewLinks(): ActiveQuery
    {
        return $this->hasMany(Link::class, [
            'view_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[Viewsession]].
     *
     * @return ActiveQuery
     */
    public function getViewsession(): ActiveQuery
    {
        return $this->hasOne(Viewsession::class, [
            'id' => 'viewsession_id',
        ]);
    }
}
