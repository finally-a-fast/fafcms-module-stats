<?php

namespace fafcms\stats\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\NumberInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\stats\{
    Bootstrap,
    models\Link,
    models\View,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%viewsession}}".
 *
 * @package fafcms\stats\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string|null $enter_at
 * @property string|null $last_focus_at
 * @property string|null $last_blur_at
 * @property string|null $leave_at
 * @property int $duration
 * @property int $useragent_id
 *
 * @property Link[] $viewsessionLinks
 * @property View[] $viewsessionViews
 */
abstract class BaseViewsession extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/viewsession';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'viewsession';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-stats', 'Viewsessions');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-stats', 'Viewsession');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'enter_at' => [
                'type' => DateTimePicker::class,
            ],
            'last_focus_at' => [
                'type' => DateTimePicker::class,
            ],
            'last_blur_at' => [
                'type' => DateTimePicker::class,
            ],
            'leave_at' => [
                'type' => DateTimePicker::class,
            ],
            'duration' => [
                'type' => NumberInput::class,
            ],
            'useragent_id' => [
                'type' => NumberInput::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'enter_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'enter_at',
                        'sort' => 2,
                    ],
                ],
                'last_focus_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_focus_at',
                        'sort' => 3,
                    ],
                ],
                'last_blur_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_blur_at',
                        'sort' => 4,
                    ],
                ],
                'leave_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'leave_at',
                        'sort' => 5,
                    ],
                ],
                'duration' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'duration',
                        'sort' => 6,
                    ],
                ],
                'useragent_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'useragent_id',
                        'sort' => 7,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-enter_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'enter_at',
                                                    ],
                                                ],
                                                'field-last_focus_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_focus_at',
                                                    ],
                                                ],
                                                'field-last_blur_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_blur_at',
                                                    ],
                                                ],
                                                'field-leave_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'leave_at',
                                                    ],
                                                ],
                                                'field-duration' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'duration',
                                                    ],
                                                ],
                                                'field-useragent_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'useragent_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%viewsession}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'date-enter_at' => ['enter_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'enter_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-last_focus_at' => ['last_focus_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'last_focus_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-last_blur_at' => ['last_blur_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'last_blur_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-leave_at' => ['leave_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'leave_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'integer-duration' => ['duration', 'integer'],
            'integer-useragent_id' => ['useragent_id', 'integer'],
            'required-useragent_id' => ['useragent_id', 'required'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-stats', 'ID'),
            'enter_at' => Yii::t('fafcms-stats', 'Enter At'),
            'last_focus_at' => Yii::t('fafcms-stats', 'Last Focus At'),
            'last_blur_at' => Yii::t('fafcms-stats', 'Last Blur At'),
            'leave_at' => Yii::t('fafcms-stats', 'Leave At'),
            'duration' => Yii::t('fafcms-stats', 'Duration'),
            'useragent_id' => Yii::t('fafcms-stats', 'Useragent ID'),
        ]);
    }

    /**
     * Gets query for [[ViewsessionLinks]].
     *
     * @return ActiveQuery
     */
    public function getViewsessionLinks(): ActiveQuery
    {
        return $this->hasMany(Link::class, [
            'viewsession_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[ViewsessionViews]].
     *
     * @return ActiveQuery
     */
    public function getViewsessionViews(): ActiveQuery
    {
        return $this->hasMany(View::class, [
            'viewsession_id' => 'id',
        ]);
    }
}
