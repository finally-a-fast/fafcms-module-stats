<?php

namespace fafcms\stats\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\TextInput,
    inputs\UrlInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\stats\{
    Bootstrap,
    models\Useragent,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%viewsummary}}".
 *
 * @package fafcms\stats\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int $period
 * @property string $period_start_at
 * @property string $period_end_at
 * @property int $statuscode
 * @property string|null $url
 * @property string|null $referrer
 * @property int|null $user_id
 * @property string|null $usergroup
 * @property int $duration
 * @property int $useragent_id
 *
 * @property Useragent $useragent
 */
abstract class BaseViewsummary extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/viewsummary';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'viewsummary';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-stats', 'Viewsummaries');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-stats', 'Viewsummary');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'useragent_id' => static function($properties = []) {
                return Useragent::getOptionProvider($properties)->getOptions();
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'period' => [
                'type' => NumberInput::class,
            ],
            'period_start_at' => [
                'type' => DateTimePicker::class,
            ],
            'period_end_at' => [
                'type' => DateTimePicker::class,
            ],
            'statuscode' => [
                'type' => NumberInput::class,
            ],
            'url' => [
                'type' => UrlInput::class,
            ],
            'referrer' => [
                'type' => TextInput::class,
            ],
            'user_id' => [
                'type' => NumberInput::class,
            ],
            'usergroup' => [
                'type' => TextInput::class,
            ],
            'duration' => [
                'type' => NumberInput::class,
            ],
            'useragent_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('useragent_id', false),
                'relationClassName' => Useragent::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'period' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'period',
                        'sort' => 2,
                    ],
                ],
                'period_start_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'period_start_at',
                        'sort' => 3,
                    ],
                ],
                'period_end_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'period_end_at',
                        'sort' => 4,
                    ],
                ],
                'statuscode' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'statuscode',
                        'sort' => 5,
                    ],
                ],
                'url' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'url',
                        'sort' => 6,
                    ],
                ],
                'referrer' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'referrer',
                        'sort' => 7,
                    ],
                ],
                'user_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'user_id',
                        'sort' => 8,
                    ],
                ],
                'usergroup' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'usergroup',
                        'sort' => 9,
                    ],
                ],
                'duration' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'duration',
                        'sort' => 10,
                    ],
                ],
                'useragent_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'useragent_id',
                        'sort' => 11,
                        'link' => true,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-period' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'period',
                                                    ],
                                                ],
                                                'field-period_start_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'period_start_at',
                                                    ],
                                                ],
                                                'field-period_end_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'period_end_at',
                                                    ],
                                                ],
                                                'field-statuscode' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'statuscode',
                                                    ],
                                                ],
                                                'field-url' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'url',
                                                    ],
                                                ],
                                                'field-referrer' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'referrer',
                                                    ],
                                                ],
                                                'field-user_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'user_id',
                                                    ],
                                                ],
                                                'field-usergroup' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'usergroup',
                                                    ],
                                                ],
                                                'field-duration' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'duration',
                                                    ],
                                                ],
                                                'field-useragent_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'useragent_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%viewsummary}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-period' => ['period', 'required'],
            'required-period_start_at' => ['period_start_at', 'required'],
            'required-period_end_at' => ['period_end_at', 'required'],
            'required-useragent_id' => ['useragent_id', 'required'],
            'integer-period' => ['period', 'integer'],
            'integer-statuscode' => ['statuscode', 'integer'],
            'integer-user_id' => ['user_id', 'integer'],
            'integer-duration' => ['duration', 'integer'],
            'integer-useragent_id' => ['useragent_id', 'integer'],
            'date-period_start_at' => ['period_start_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'period_start_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-period_end_at' => ['period_end_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'period_end_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-url' => ['url', 'string', 'max' => 5000],
            'string-referrer' => ['referrer', 'string', 'max' => 5000],
            'string-usergroup' => ['usergroup', 'string', 'max' => 255],
            'exist-useragent_id' => [['useragent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Useragent::class, 'targetAttribute' => ['useragent_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-stats', 'ID'),
            'period' => Yii::t('fafcms-stats', 'Period'),
            'period_start_at' => Yii::t('fafcms-stats', 'Period Start At'),
            'period_end_at' => Yii::t('fafcms-stats', 'Period End At'),
            'statuscode' => Yii::t('fafcms-stats', 'Statuscode'),
            'url' => Yii::t('fafcms-stats', 'Url'),
            'referrer' => Yii::t('fafcms-stats', 'Referrer'),
            'user_id' => Yii::t('fafcms-stats', 'User ID'),
            'usergroup' => Yii::t('fafcms-stats', 'Usergroup'),
            'duration' => Yii::t('fafcms-stats', 'Duration'),
            'useragent_id' => Yii::t('fafcms-stats', 'Useragent ID'),
        ]);
    }

    /**
     * Gets query for [[Useragent]].
     *
     * @return ActiveQuery
     */
    public function getUseragent(): ActiveQuery
    {
        return $this->hasOne(Useragent::class, [
            'id' => 'useragent_id',
        ]);
    }
}
