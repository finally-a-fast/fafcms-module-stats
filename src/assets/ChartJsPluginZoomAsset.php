<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\assets;

use yii\web\AssetBundle;

/**
 * Class ChartJsPluginZoomAsset
 * @package fafcms\stats\assets
 */
class ChartJsPluginZoomAsset extends AssetBundle
{
    public $sourcePath = '@npm/chartjs-plugin-zoom/dist';

    public $js = [
        'chartjs-plugin-zoom.min.js'
    ];

    public $depends = [
        'fafcms\stats\assets\ChartJsAsset',
        'fafcms\stats\assets\HammerJsAsset'
    ];
}
