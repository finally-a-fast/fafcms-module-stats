<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\assets;

use fafcms\fafcms\assets\LuxonAsset;
use yii\web\AssetBundle;

/**
 * Class LuxonAdapterAsset
 *
 * @package fafcms\stats\assets
 */
class LuxonAdapterAsset extends AssetBundle
{
    public $sourcePath = '@npm/chartjs-adapter-luxon/dist';

    public $js = [
        'chartjs-adapter-luxon.min.js'
    ];

    public $depends = [
        ChartJsAsset::class,
        LuxonAsset::class
    ];
}
