<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\stats\assets;

use fafcms\fafcms\assets\fafcms\FafcmsAppAsset;
use fafcms\helpers\classes\AssetComponentBundle;

/**
 * Class PageActionAsset
 *
 * @package fafcms\stats\assets
 */
class PageActionAsset extends AssetComponentBundle
{
    public $sourcePath = __DIR__ . '/page-action';

    public $js = [
        'page-action.js',
    ];

    public $depends = [
        FafcmsAppAsset::class,
    ];
}
