function PageAction () {
  let cmsModule = this

  let config = Object.assign({
    basic: false,
    link: true,
    linkSelector: '.fafcms-page-action-link'
  }, window.faf.config['page-action'])

  if (config.basic) {
    window.faf.events['windowBeforeUnload'].push(function () {
      cmsModule.save('leave')
    })

    window.faf.events['windowBlur'].push(function () {
      cmsModule.save('blur')
    })

    window.faf.events['windowFocus'].push(function () {
      cmsModule.save('focus')
    })
  }

  if (config.link) {
    let linkSelector = config.linkSelector

    document.addEventListener('click', function(e) {
      for (let target = e.target; target && target !== this; target = target.parentNode) {
        if (target.matches(linkSelector)) {
          cmsModule.save('link', {
            'url': target.getAttribute('href') || target.dataset.src || target.dataset.href
          })
          break;
        }
      }
    }, false);
  }

  cmsModule.save = function (action, options) {
    let requestUrl = new URLSearchParams()
    requestUrl.set('time', new Date().toISOString())
    requestUrl.set('action', action)

    if (typeof config.id !== 'undefined') {
      requestUrl.set('id', config.id)
    } else if(action !== 'link') {
      return;
    }

    if (typeof options !== 'undefined') {
      for (let optionName in options) {
        requestUrl.set(optionName, options[optionName])
      }
    }

    let img = document.createElement('img')
    img.onload = function () {
      img.remove()
    }
    img.src = config.url + '?' + requestUrl.toString()

    document.body.appendChild(img)
  }
}
