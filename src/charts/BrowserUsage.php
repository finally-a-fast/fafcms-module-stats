<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\charts;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\ExtendedDropDownList;
use fafcms\parser\elements\JsonEncode;
use fafcms\stats\classes\Chart;
use fafcms\stats\classes\ChartContent;
use fafcms\stats\classes\ChartSetting;
use fafcms\stats\models\Useragent;
use Mexitek\PHPColors\Color;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\web\JsExpression;

/**
 * Class BrowserUsage
 * @package fafcms\stats\charts
 */
class BrowserUsage extends Chart
{
    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
        return Yii::t('fafcms-stats', 'Browser usage');
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-stats', 'Shows browser usage.');
    }

    /**
     * {@inheritdoc}
     */
    public function chartSettings(): ?array
    {
        return [
            new ChartSetting($this, [
                'label' => Yii::t('fafcms-stats', 'Type'),
                'name' => 'type',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => ExtendedDropDownList::class,
                'inputOptions' => [
                    ChartContent::TYPE_PIE => 'Pie',
                    ChartContent::TYPE_DOUGHNUT => 'Doughnut',
                ],
                'defaultValue' => ChartContent::TYPE_PIE,
            ]),
            new ChartSetting($this, [
                'label' => Yii::t('fafcms-stats', 'Group versions'),
                'name' => 'groupVersions',
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'inputType' => Checkbox::class,
                'defaultValue' => true,
            ]),
            new ChartSetting($this, [
                'label' => Yii::t('fafcms-stats', 'Group browsers under x precent'),
                'name' => 'groupBrowsersUnder',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class,
                'defaultValue' => 6,
            ]),
            new ChartSetting($this, [
                'label' => Yii::t('fafcms-stats', 'Show last x days'),
                'name' => 'lastDays',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class,
                'defaultValue' => 14,
            ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function run(): ChartContent
    {
        $labels = [];
        $browserDatas = [];
        $browserBackgroundColors = [];
        $browserColorsCount = [];

        $labelColors = [];

        $browserColors = [
            'Chrome' => '#81c784',
            'Chrome Mobile' => '#aed581',
            'Firefox' => '#ffb74d',
            'Firefox Mobile' => '#ff6f00',
            'Internet Explorer' => '#ffd54f',
            'Microsoft Edge' => '#4dd0e1',
            'Microsoft Edge Mobile' => '#4db6ac',
            'Opera' => '#e57373',
            'Opera Mobile' => '#f06292',
            'Safari' => '#64b5f6',
            'Safari Mobile' => '#4fc3f7',
            'Vivaldi' => '#90a4ae'
        ];

        $systemColors = [
            'desktop'               => '#eeeeee',
            'smartphone'            => '#dddddd',
            'tablet'                => '#cccccc',
            'feature phone'         => '#bbbbbb',
            'console'               => '#aaaaaa',
            'tv'                    => '#999999',
            'car browser'           => '#888888',
            'smart display'         => '#777777',
            'camera'                => '#666666',
            'portable media player' => '#555555',
            'phablet'               => '#444444',
            'smart speaker'         => '#333333',
            'wearable'              => '#222222',
        ];

        $nameAliases = [
            'smartphone' => [
                'Microsoft Edge' => 'Microsoft Edge Mobile',
                'Mobile Safari' => 'Safari Mobile'
            ]
        ];

        $browsers = Useragent::find()
            ->select([
                'COUNT(*) AS usage',
                Useragent::tableName().'.device_name',
                Useragent::tableName().'.browser_name'
            ])
            ->innerJoinWith('viewsessions', false)
            ->groupBy([
                Useragent::tableName().'.device_name',
                Useragent::tableName().'.browser_name'
            ])
            ->orderBy([
                Useragent::tableName().'.device_name' => 'ASC',
                Useragent::tableName().'.browser_name' => 'ASC'
            ]);

        if (!$this->getSetting('groupVersions')) {
            $browsers->addSelect([Useragent::tableName().'.browser_version']);
            $browsers->addGroupBy([Useragent::tableName().'.browser_version']);
            $browsers->addOrderBy([Useragent::tableName().'.browser_version' => 'ASC']);
        }

        $browsers = $browsers->asArray()->all();

        $total = array_sum(ArrayHelper::getColumn($browsers, 'usage'));

        foreach ($browsers as $browser) {
            if (($browser['usage']  * 100) / $total < $this->getSetting('groupBrowsersUnder')) {
                if (!isset($browserDatas[$browser['device_name']])) {
                    $labels[] = Yii::t('fafcms-stats', 'Other').' '.Inflector::humanize($browser['device_name']);
                    $browserDatas[$browser['device_name']] = 0;
                    $browserColor = $systemColors[$browser['device_name']]??'#795548';

                    $browserBackgroundColors[] = $browserColor;

                    $browserColorObject = new Color($browserColor);

                    if ($browserColorObject->isDark()) {
                        $labelColors[] = '#ffffff';
                    } else {
                        $labelColors[] = '#000000';
                    }
                }

                $browserDatas[$browser['device_name']] += (int)$browser['usage'];
            } else {
                $name = $nameAliases[$browser['device_name']][$browser['browser_name']]??$browser['browser_name'];
                $labels[] = $name.(!$this->getSetting('groupVersions')?' ('.$browser['browser_version'].')':'');
                $browserDatas[] = (int)$browser['usage'];
                $browserColor = $browserColors[$name];

                if (!isset($browserColorsCount[$name])) {
                    $browserColorsCount[$name] = 0;
                } else {
                    $browserColorsCount[$name] += 5;

                    $browserColor = new Color($browserColor);

                    if ($browserColor->isDark()) {
                        $browserColor = '#'.$browserColor->lighten($browserColorsCount[$name]);
                    } else {
                        $browserColor = '#'.$browserColor->darken($browserColorsCount[$name]);
                    }
                }

                $browserColorObject = new Color($browserColor);

                if ($browserColorObject->isDark()) {
                    $labelColors[] = '#ffffff';
                } else {
                    $labelColors[] = '#000000';
                }

                $browserBackgroundColors[] = $browserColor;
            }
        }

        $browserDatas = array_values($browserDatas);

        $systemDatas = [];
        $systemTypes = [];

        for ($i = 0, $iMax = count($browserDatas); $i < $iMax; $i++) {
            $systemDatas[] = 0;
        }

        foreach ($browsers as $browser) {
            if (!isset($systemTypes[$browser['device_name']])) {
                $systemTypes[$browser['device_name']] = [
                    'count' => 0,
                    'label' => Inflector::humanize($browser['device_name'])
                ];

                $browserColor = $systemColors[$browser['device_name']]??'#795548';
                $browserBackgroundColors[] = $browserColor;

                $browserColorObject = new Color($browserColor);

                if ($browserColorObject->isDark()) {
                    $labelColors[] = '#ffffff';
                } else {
                    $labelColors[] = '#000000';
                }

                $browserDatas[] = 0;
            }

            $systemTypes[$browser['device_name']]['count'] += $browser['usage'];
        }

        $labels = array_merge($labels, ArrayHelper::getColumn($systemTypes, 'label', false));
        $systemDatas = array_merge($systemDatas, ArrayHelper::getColumn($systemTypes, 'count', false));

        $datasets = [
            [
                'label' => 'Browser usage',
                'backgroundColor' => $browserBackgroundColors,
                'data' => $browserDatas
            ],
            [
                'label' => 'System usage',
                'backgroundColor' => $browserBackgroundColors,
                'data' => $systemDatas
            ],
        ];

        $options = [
            'plugins' => [
                'datalabels' => [
                    'formatter' => new JsExpression('function(value, context) {
                        var sum = 0;
                        var dataArr = context.chart.data.datasets[0].data;

                        if (value === null) {
                            value = 0
                        }

                        dataArr.map(function(data) {
                            if (data === null) {
                                data = 0
                            }

                            sum += Number.parseFloat(data);
                        });

                        var percentage = (value * 100 / sum).toFixed(0);

                        if (percentage == 0) {
                            return \'\'
                        }

                        return percentage + \'%\'
                    }'),
                    'color' => $labelColors
                ]
            ]
        ];

        return new ChartContent([
            'labels' => $labels,
            'datasets' => $datasets,
            'type' => $this->getSetting('type'),
            'options' => $options
        ]);
    }
}
