<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\charts;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\NumberInput;
use fafcms\fafcms\inputs\ExtendedDropDownList;
use fafcms\stats\classes\Chart;
use fafcms\stats\classes\ChartContent;
use fafcms\stats\classes\ChartSetting;
use fafcms\stats\models\View;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class PageViews
 * @package fafcms\stats\charts
 */
class PageViews extends Chart
{
    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
        return Yii::t('fafcms-stats', 'Page views');
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return Yii::t('fafcms-stats', 'Page views.');
    }

    /**
     * {@inheritdoc}
     */
    public function chartSettings(): ?array
    {
        return [
            new ChartSetting($this, [
                'label' => Yii::t('fafcms-stats', 'Type'),
                'name' => 'type',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => ExtendedDropDownList::class,
                'inputOptions' => [
                    ChartContent::TYPE_LINE => 'Line',
                    ChartContent::TYPE_BAR => 'Bar',
                ],
                'defaultValue' => ChartContent::TYPE_LINE,
            ]),
            new ChartSetting($this, [
                'label' => Yii::t('fafcms-stats', 'Show last x days'),
                'name' => 'lastDays',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class,
                'defaultValue' => 14,
            ]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function run(): ChartContent
    {
        $labels = [];
        $visitDatas = [];
        $viewDatas = [];
        $uniqueViewDatas = [];

        $visits = View::find()->select([
            'DATE_FORMAT(enter_at, "%d-%m-%Y") AS date'
        ])
        ->groupBy([
            'DATE_FORMAT(enter_at, "%d-%m-%Y")',
            'viewsession_id'
        ]);

        $query = new Query();
        $visits = $query->select([
                'COUNT(*) AS usage',
                'date'
            ])
            ->from(new Expression('('.$visits->createCommand()->getRawSql().') AS views'))
            ->groupBy(['date'])
            ->orderBy(['date' => 'ASC'])
            ->all();

        $visits = ArrayHelper::index($visits, 'date');

        $views = View::find()->select([
            'COUNT(*) AS usage',
            'DATE_FORMAT(enter_at, "%d-%m-%Y") AS date'
        ])->groupBy([
            'DATE_FORMAT(enter_at, "%d-%m-%Y")'
        ])->orderBy([
            'DATE_FORMAT(enter_at, "%d-%m-%Y")' => 'ASC'
        ]);

        $views = ArrayHelper::index($views->asArray()->all(), 'date');

        $uniqueViews = View::find()->select([
            'DATE_FORMAT(enter_at, "%d-%m-%Y") AS date'
        ])
        ->groupBy([
            'DATE_FORMAT(enter_at, "%d-%m-%Y")',
            'viewsession_id',
            'url'
        ]);

        $query = new Query();
        $uniqueViews = $query->select([
                'COUNT(*) AS usage',
                'date'
            ])
            ->from(new Expression('('.$uniqueViews->createCommand()->getRawSql().') AS views'))
            ->groupBy(['date'])
            ->orderBy(['date' => 'ASC'])
            ->all();

        $uniqueViews = ArrayHelper::index($uniqueViews, 'date');

        for ($i = $this->getSetting('lastDays') - 1; $i >= 0; $i--)
        {
            $date = date('d-m-Y', strtotime($i.' days ago'));

            $labels[] = Yii::$app->formatter->asDate($date);
            $visitDatas[] = (int)($visits[$date]['usage']??0);
            $viewDatas[] = (int)($views[$date]['usage']??0);
            $uniqueViewDatas[] = (int)($uniqueViews[$date]['usage']??0);
        }

        $datasets = [
            [
                'label' => 'Visits',
                'data' => $visitDatas,
                'backgroundColor' => '#243239',
                'borderColor' => '#243239',
                'fill' => false
            ],
            [
                'label' => 'Unique Pageviews',
                'data' => $uniqueViewDatas,
                'backgroundColor' => '#657074',
                'borderColor' => '#657074',
                'fill' => false
            ],
            [
                'label' => 'Pageviews',
                'data' => $viewDatas,
                'backgroundColor' => '#d1d1d1',
                'borderColor' => '#d1d1d1',
                'fill' => false
            ],
        ];

        return new ChartContent([
            'labels' => $labels,
            'datasets' => $datasets,
            'type' => $this->getSetting('type'),
        ]);
    }
}
