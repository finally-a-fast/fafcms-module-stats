<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\classes;

use yii\base\BaseObject;
use yii\helpers\ArrayHelper;

/**
 * Class Chart
 * @package fafcms\stats\classes
 */
abstract class Chart extends BaseObject
{
    /**
     * @return string
     */
    abstract public function label(): string;

    /**
     * @return string
     */
    abstract public function description(): string;

    /**
     * @var array
     */
    public $settings = [];

    /**
     * @return ChartSetting[]
     */
    public function chartSettings(): ?array
    {
        return null;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getSetting(string $name)
    {
        $settings = ArrayHelper::index($this->chartSettings()??[], 'name');

        if (isset($settings[$name])) {
            return $settings[$name]->getValue();
        }

        return null;
    }

    /**
     * @return ChartContent
     */
    abstract public function run(): ChartContent;
}
