<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\classes;

use yii\base\BaseObject;

/**
 * Class ChartContent
 * @package fafcms\stats\classes
 */
class ChartContent extends BaseObject
{
    public const TYPE_LINE = 'line';
    public const TYPE_BAR = 'bar';
    public const TYPE_RADAR = 'radar';
    public const TYPE_POLAR_AREA = 'polararea';
    public const TYPE_PIE = 'pie';
    public const TYPE_DOUGHNUT = 'doughnut';

    /**
     * @var array
     */
    public $labels = [];

    /**
     * @var array
     */
    public $datasets = [];

    /**
     * @var array
     */
    public $options = [];

    /**
     * @var string
     */
    public $type = self::TYPE_PIE;
}
