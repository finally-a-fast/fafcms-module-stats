<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\classes;

use fafcms\helpers\abstractions\Setting;
use Closure;

/**
 * Class ChartSetting
 * @package fafcms\stats\classes
 *
 * @property string $id
 * @property mixed $value
 */
class ChartSetting extends Setting
{
    /**
     * @var Chart
     */
    public $chart;

    /**
     * {@inheritdoc}
     */
    public function __construct(Chart $chart, $config = [])
    {
        $this->chart = $chart;
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function setValue($value, ...$params): bool
    {
        $this->chart->settings[$this->name] = $value;
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getValue(...$params)
    {
        if (!isset($this->chart->settings[$this->name])) {
            if ($this->defaultValue instanceof Closure) {
                $this->chart->settings[$this->name] = call_user_func($this->defaultValue, $this);
            } else {
                $this->chart->settings[$this->name] = $this->defaultValue;
            }
        }

        return $this->chart->settings[$this->name];
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): string
    {
        return $this->name;
    }
}
