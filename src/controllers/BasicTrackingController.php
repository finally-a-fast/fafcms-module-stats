<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\controllers;

use fafcms\stats\models\Link;
use fafcms\stats\models\View;
use fafcms\stats\models\Viewsession;
use fafcms\stats\Module;
use Yii;
use yii\web\Controller;
use DateTime;
use DateTimeZone;

/**
 * Class BasicTrackingController
 * @package fafcms\stats\controllers
 */
class BasicTrackingController extends Controller
{
    public function actionApi()
    {
        $action = Yii::$app->request->get('action');
        $id = Yii::$app->request->get('id');
        $time = Yii::$app->request->get('time');

        $now = new DateTime('NOW', new DateTimeZone(Yii::$app->formatter->defaultTimeZone));

        if ($time !== null && preg_match('/^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/', $time) > 0) {
            $time = new DateTime($time);

            if (Yii::$app->fafcms->getTotalInterval($now->diff($time), 'minutes') > 10) {
                $time = $now;
            }
        } else {
            $time = $now;
        }

        $time = $time->format('Y-m-d H:i:s');

        if ($action !== null) {
            $module = Module::getLoadedModule();
            $viewSession = null;
            $view = null;

            if (($viewSessionId = Yii::$app->getSession()->get('basic-tracking-viewsession-id')) !== null) {
                $viewSession = Viewsession::find()->where(['hashId' => $viewSessionId])->one();

                if ($viewSession !== null && $id !== null) {
                    $view = View::find()->where([
                        'hashId' => $id,
                        'viewsession_id' => $viewSession->id,
                        'leave_at' => null
                    ])->one();
                }
            }

            if ($action === 'link' && $module->getPluginSettingValue('enable_link_tracking')) {
                $url = Yii::$app->request->get('url');

                if ($url !== null) {
                    $data = [
                        'url' => $url,
                        'referrer' => str_replace(['https://' . Yii::$app->fafcms->getCurrentProjectLanguage()->domain->domain, 'http://' . Yii::$app->fafcms->getCurrentProjectLanguage()->domain->domain], '', Yii::$app->request->referrer),
                        'view_id' => $view->id ?? null,
                        'viewsession_id' => $viewSession->id ?? null,
                        'user_id' => (Yii::$app->user->isGuest ?null : Yii::$app->user->id),
                        'usergroup' => (Yii::$app->user->isGuest ? null : Yii::$app->user->identity->getUserGroupLabel()),
                        'click_at' => $time,
                    ];

                    if (!Link::find()->where($data)->exists()) {
                        $link = new Link();
                        $link->loadDefaultValues();
                        $link->setAttributes($data);

                        if (!$link->save()) {
                            Yii::error('Cannot save link click. Errors: ' . print_r($link->getErrors(), true));
                        }
                    }
                }
            } elseif ($view !== null && $module->getPluginSettingValue('enable_basic_tracking')) {
                switch ($action) {
                    case 'leave':
                        $view->endPageView($time);
                        $viewSession->endPageView($time);
                        break;
                    case 'focus':
                        $view->startPageView($time);
                        $viewSession->startPageView($time);
                        break;
                    case 'blur':
                        $view->stopPageView($time);
                        $viewSession->stopPageView($time);
                        break;
                }
            }
        }

        header('Expires: Thu, 01 Jan 1970 00:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0', false);
        header('Pragma: no-cache');

        return Yii::$app->response->sendFile(Yii::getAlias('@fafcms/stats/assets/img/transparent.png'), '42.png', [
            'mimeType' => 'image/png',
            'inline' => true
        ]);
    }
}
