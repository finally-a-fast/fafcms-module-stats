<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\controllers;

use fafcms\helpers\ClassFinder;
use fafcms\stats\classes\Chart;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\AccessControl;

/**
 * Class ChartController
 * @package fafcms\stats\controllers
 */
class ChartController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => Yii::$app->fafcms->accessRules['default'],
            ],
        ];
    }

    public function actionApi()
    {
        //todo
        return null;
        Yii::$app->response->format = Response::FORMAT_JSON;

        $finder = new ClassFinder();

        $filter = [
            'extends' => Chart::class,
        ];

        $chartClasses = [
            $finder->findClasses(Yii::getAlias('@project/charts'), $filter)
        ];

        foreach (Yii::$app->fafcms->getLoadedPluginPaths() as $loadedPluginPath) {
            $chartClasses[] = $finder->findClasses($loadedPluginPath.'/charts', $filter);
        }

        $chartClasses = array_merge([], ...$chartClasses);
        $validChartClasses = ArrayHelper::getColumn($chartClasses, 'fullyQualifiedClassName');

        if (($datasourceClass = Yii::$app->request->get('datasource')) === null || !in_array($datasourceClass, $validChartClasses, true)) {
            throw new NotFoundHttpException('Unknown chart');
        }

        $datasource = new $datasourceClass;

        $data = $datasource->run();

        /*var_dump($data->options);
        var_dump(json_encode($data->options));
        var_dump(Json::htmlEncode($data->options));*/
        //die();
        //Json::htmlEncode()

        $options = $data->options;

        $options = ArrayHelper::merge([
            'plugins' => [
                'datalabels' => false,
                'zoom' => [
                    'pan' => [
                        'enabled' => true,
                        'mode' =>  'xy'
                    ],
                    'zoom' => [
                        'enabled' => true,
                        'mode' =>  'xy'
                    ]
                ]
            ]
        ], $options);

        try {
            return [
                'type' => $data->type,
                'labelsHash' => hash_hmac(Yii::$app->getSecurity()->macHash, json_encode($data->labels, JSON_THROW_ON_ERROR), ''),
                'labels' => $data->labels,
                'datasetsHash' => hash_hmac(Yii::$app->getSecurity()->macHash, json_encode($data->datasets, JSON_THROW_ON_ERROR), ''),
                'datasets' => $data->datasets,
                'optionsHash' => hash_hmac(Yii::$app->getSecurity()->macHash, json_encode($options, JSON_THROW_ON_ERROR), ''),
                'options' => '(function(){return '.Json::encode($options).';})()',
            ];
        } catch (\JsonException $e) {
        }

        return null;
    }
}
