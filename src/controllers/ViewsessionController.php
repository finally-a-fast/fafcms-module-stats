<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\controllers;

use fafcms\helpers\DefaultController;
use fafcms\stats\models\Viewsession;

/**
 * Class ViewsessionController
 *
 * @package fafcms\stats\controllers
 */
class ViewsessionController extends DefaultController
{
    public static $modelClass = Viewsession::class;
}
