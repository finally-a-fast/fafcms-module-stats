<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\items;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\inputs\NumberInput;
use fafcms\fafcms\inputs\ExtendedDropDownList;
use fafcms\fafcms\inputs\SwitchCheckbox;
use fafcms\fafcms\inputs\TextInput;
use fafcms\helpers\classes\ContentItemSetting;
use fafcms\stats\assets\ChartJsAsset;
use fafcms\stats\assets\ChartJsPluginDatalabelsAsset;
use fafcms\stats\assets\ChartJsPluginZoomAsset;
use fafcms\stats\assets\LuxonAdapterAsset;
use fafcms\stats\Bootstrap;
use Yii;
use fafcms\helpers\abstractions\ContentItem;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\Html;

/**
 * Class Chart
 * @package fafcms\stats\items
 */
class Chart extends ContentItem
{
    public int $type = self::TYPE_ITEM;

    public function label(): string
    {
        return Yii::t('fafcms-core', 'Chart');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Show chart.');
    }

    public function itemSettings(): ?array
    {
        return [
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Datasource'),
                'name' => 'datasource',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => ExtendedDropDownList::class,
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Height'),
                'name' => 'height',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class,
                'defaultValue' => 400,
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Width'),
                'name' => 'width',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class,
                'defaultValue' => 400,
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Auto update'),
                'name' => 'autoUpdate',
                'inputType' => SwitchCheckbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'defaultValue' => true,
                'fieldConfig' => [
                    'offLabel' => Yii::t('fafcms-core', 'No'),
                    'onLabel' => Yii::t('fafcms-core', 'Yes')
                ]
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Auto update interval'),
                'name' => 'autoUpdateInterval',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class,
                'defaultValue' => 30,
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Id'),
                'name' => 'id',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class
            ]),
        ];
    }

    public function run(): string
    {
        $id = $this->getSetting('id');

        if ($id === null) {
            try {
                $id = 'chartjs-' . random_int(0, PHP_INT_MAX);
            } catch (\Exception $e) {
                return '';
            }
        }

        $view = Yii::$app->getView();

        ChartJsAsset::register($view);
        LuxonAdapterAsset::register($view);
        ChartJsPluginZoomAsset::register($view);
        ChartJsPluginDatalabelsAsset::register($view);

        $chart = Html::tag('canvas', '', [
            'id' => $id,
            'height' => $this->getSetting('height'),
            'width' => $this->getSetting('width'),
            'role' => 'img',
            'aria-label' => 'Chart',
        ]);

        $chartApi = Url::to(['/'.Bootstrap::$id.'/chart/api']);
        $update = '';

        if ($this->getSetting('autoUpdate')) {
            //$update = 'setInterval(getData, '.($this->getSetting('autoUpdateInterval') * 1000).')';
        }

        $datasource = Json::encode($this->getSetting('datasource'));

        $view->registerJs(<<<JS
(function() {
    var chart
    var oldLabels
    var oldDatasets
    var oldType
    var oldOptions

    var getData = function() {
        $.ajax({
            url: '$chartApi',
            data: {
                datasource: $datasource
            },
            success: function(data) {
                if (typeof chart === 'undefined' || oldType !== data.type) {
                    oldType = data.type
                    oldLabels = data.labelsHash
                    oldDatasets = data.datasetsHash
                    oldOptions = data.optionsHash

                    var options = {}

                    if (typeof data.options !== 'undefined') {
                        options = window.eval(data.options)
                    }

                    chart = new Chart($('#$id'), {
                        'type': data.type,
                        'data': {
                            labels: data.labels,
                            datasets: data.datasets,
                        },
                        options: options
                    })
                } else {
                    if (oldLabels !== data.labelsHash) {
                        oldLabels = data.labelsHash
                        chart.data.labels = data.labels
                        chart.update()
                    }

                    if (oldDatasets !== data.datasetsHash) {
                        oldDatasets = data.datasetsHash
                        chart.data.datasets = data.datasets
                        chart.update()
                    }

                    if (oldOptions !== data.optionsHash) {
                        oldOptions = data.optionsHash

                        var options = {}

                        if (typeof data.options !== 'undefined') {
                            options = window.eval(data.options)
                        }

                        chart.options = options
                        chart.update()
                    }
                }
            }
        })
    }

    getData()
    $update
})();
JS);

        return $chart;
    }
}
