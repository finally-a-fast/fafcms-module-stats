<?php

namespace fafcms\stats\migrations;

use fafcms\stats\models\Useragent;
use fafcms\stats\models\View;
use fafcms\stats\models\Viewsession;
use fafcms\stats\models\Viewsummary;
use yii\db\Migration;

/**
 * Class m200508_124422_tracking
 * @package fafcms\stats\migrations
 */
class m200508_124422_tracking extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Useragent::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'agent' => $this->string(255)->null()->defaultValue(null),
            'device_name' => $this->string(255)->null()->defaultValue(null),
            'brand_name' => $this->string(255)->null()->defaultValue(null),
            'model' => $this->string(255)->null()->defaultValue(null),
            'os_name' => $this->string(255)->null()->defaultValue(null),
            'os_version' => $this->string(255)->null()->defaultValue(null),
            'browser_name' => $this->string(255)->null()->defaultValue(null),
            'browser_version' => $this->string(255)->null()->defaultValue(null),
        ], $tableOptions);

        $this->createTable(Viewsession::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'enter_at' => $this->datetime()->null()->defaultValue(null),
            'last_focus_at' => $this->datetime()->null()->defaultValue(null),
            'last_blur_at' => $this->datetime()->null()->defaultValue(null),
            'leave_at' => $this->datetime()->null()->defaultValue(null),
            'duration' => $this->integer(10)->unsigned()->defaultValue(0)->notNull(),
            'useragent_id' => $this->integer(10)->unsigned()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-viewsession-useragent_id', Viewsession::tableName(), ['useragent_id'], false);
        $this->addForeignKey('fk-viewsession-useragent_id', Viewsession::tableName(), 'useragent_id', Useragent::tableName(), 'id', 'RESTRICT', 'CASCADE');

        $this->createTable(View::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'statuscode' => $this->smallInteger(3)->unsigned()->defaultValue(200)->notNull(),
            'url' => $this->string(5000)->null()->defaultValue(null),
            'referrer' => $this->string(5000)->null()->defaultValue(null),
            'viewsession_id' => $this->integer(10)->unsigned()->notNull(),
            'user_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'usergroup' => $this->string(255)->null()->defaultValue(null),
            'enter_at' => $this->datetime()->null()->defaultValue(null),
            'last_focus_at' => $this->datetime()->null()->defaultValue(null),
            'last_blur_at' => $this->datetime()->null()->defaultValue(null),
            'leave_at' => $this->datetime()->null()->defaultValue(null),
            'duration' => $this->integer(10)->unsigned()->defaultValue(0)->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-view-useragent_id', View::tableName(), ['viewsession_id'], false);
        $this->addForeignKey('fk-view-useragent_id', View::tableName(), 'viewsession_id', Viewsession::tableName(), 'id', 'CASCADE', 'CASCADE');

        $this->createTable(Viewsummary::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'period' => $this->integer(10)->unsigned()->notNull(),
            'period_start_at' => $this->datetime()->unsigned()->notNull(),
            'period_end_at' => $this->datetime()->unsigned()->notNull(),
            'statuscode' => $this->smallInteger(3)->unsigned()->defaultValue(200)->notNull(),
            'url' => $this->string(5000)->null()->defaultValue(null),
            'referrer' => $this->string(5000)->null()->defaultValue(null),
            'user_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'usergroup' => $this->string(255)->null()->defaultValue(null),
            'duration' => $this->integer(10)->unsigned()->defaultValue(0)->notNull(),
            'useragent_id' => $this->integer(10)->unsigned()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-viewsummary-user_id', Viewsummary::tableName(), ['user_id'], false);
        $this->createIndex('idx-viewsummary-useragent_id', Viewsummary::tableName(), ['useragent_id'], false);
        $this->addForeignKey('fk-viewsummary-useragent_id', Viewsummary::tableName(), 'useragent_id', Useragent::tableName(), 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-viewsession-useragent_id', Viewsession::tableName());
        $this->dropForeignKey('fk-view-useragent_id', View::tableName());
        $this->dropForeignKey('fk-viewsummary-useragent_id', Viewsummary::tableName());

        $this->dropTable(Useragent::tableName());
        $this->dropTable(Viewsession::tableName());
        $this->dropTable(View::tableName());
        $this->dropTable(Viewsummary::tableName());
    }
}
