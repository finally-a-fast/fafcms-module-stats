<?php

namespace fafcms\stats\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\stats\abstracts\models\BaseLink;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%link}}".
 *
 * @package fafcms\stats\models
 */
class Link extends BaseLink
{
    use MultilingualTrait;

    public bool $fafcmsLogChange = false;
}
