<?php

namespace fafcms\stats\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\stats\abstracts\models\BaseLinksummary;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%linksummary}}".
 *
 * @package fafcms\stats\models
 */
class Linksummary extends BaseLinksummary
{
    use MultilingualTrait;

    public bool $fafcmsLogChange = false;
}
