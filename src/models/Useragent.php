<?php

namespace fafcms\stats\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\stats\abstracts\models\BaseUseragent;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%useragent}}".
 *
 * @package fafcms\stats\models
 */
class Useragent extends BaseUseragent
{
    use MultilingualTrait;

    public bool $fafcmsLogChange = false;
}
