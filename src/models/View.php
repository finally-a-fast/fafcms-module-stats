<?php

namespace fafcms\stats\models;

use fafcms\helpers\traits\MultilingualTrait;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\log\Logger;
use fafcms\stats\abstracts\models\BaseView;

/**
 * This is the model class for table "{{%view}}".
 *
 * @package fafcms\stats\models
 */
class View extends BaseView
{
    use MultilingualTrait;

    public bool $fafcmsLogChange = false;

    public function startPageView(string $time)
    {
        $this->last_focus_at = $time;

        if (!$this->save(false)) {
            Yii::$app->getLog()->getLogger()->log('Cannot update page view. ' . print_r($this->getErrors(), true), Logger::LEVEL_ERROR);
            return false;
        }

        return true;
    }

    public function stopPageView($time)
    {
        $this->last_blur_at = $time;
        $this->duration = new Expression('duration + IF(TIME_TO_SEC(TIMEDIFF(:time, last_focus_at)) > 0, TIME_TO_SEC(TIMEDIFF(:time, last_focus_at)), 0)', [':time' => $time]);

        if (!$this->save(false)) {
            Yii::$app->getLog()->getLogger()->log('Cannot update page view. ' . print_r($this->getErrors(), true), Logger::LEVEL_ERROR);
            return false;
        }

        return true;
    }

    public function endPageView($time)
    {
        $this->leave_at = $time;
        $this->duration = new Expression('duration + IF(IF(last_blur_at IS NULL, TIME_TO_SEC(TIMEDIFF(:time, enter_at)), IF(last_focus_at > last_blur_at, TIME_TO_SEC(TIMEDIFF(:time, last_blur_at)), 0)) > 0, IF(last_blur_at IS NULL, TIME_TO_SEC(TIMEDIFF(:time, enter_at)), IF(last_focus_at > last_blur_at, TIME_TO_SEC(TIMEDIFF(:time, last_blur_at)), 0)), 0)', [':time' => $time]);

        if (!$this->save(false)) {
            Yii::$app->getLog()->getLogger()->log('Cannot update page view. ' . print_r($this->getErrors(), true), Logger::LEVEL_ERROR);
            return false;
        }

        return true;
    }
}
