<?php

namespace fafcms\stats\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\stats\abstracts\models\BaseViewsummary;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%viewsummary}}".
 *
 * @package fafcms\stats\models
 */
class Viewsummary extends BaseViewsummary
{
    use MultilingualTrait;

    public bool $fafcmsLogChange = false;
}
