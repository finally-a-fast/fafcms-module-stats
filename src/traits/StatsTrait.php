<?php

namespace fafcms\stats\traits;

use fafcms\stats\Bootstrap;
use Yii;

/**
 * Trait StatsTrait
 * @package fafcms\stats\traits
 */
trait StatsTrait
{
    /**
     * @return array
     */
    public function getDefaultEditViewItemsStatsTrait(): array
    {
        return Yii::$app->getModule(Bootstrap::$id)->defaultEditViewItems;
    }
}
