<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\updates\migrations;

use fafcms\fafcms\models\User;
use fafcms\stats\models\Link;
use fafcms\stats\models\Linksummary;
use fafcms\stats\models\View;
use fafcms\stats\models\Viewsession;
use fafcms\updater\base\Migration;

/**
 * Class m201026_153146_link_tracking
 *
 * @package fafcms\stats\updates\migrations
 */
class m201026_153146_link_tracking extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Link::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'url' => $this->string(5000)->null()->defaultValue(null),
            'referrer' => $this->string(5000)->null()->defaultValue(null),
            'view_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'viewsession_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'user_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'usergroup' => $this->string(255)->null()->defaultValue(null),
            'click_at' => $this->datetime()->null()->defaultValue(null)
        ], $tableOptions);

        $this->createIndex('idx-link-url', Link::tableName(), ['url'], false);
        $this->createIndex('idx-link-referrer', Link::tableName(), ['referrer'], false);
        $this->createIndex('idx-link-user_id', Link::tableName(), ['user_id'], false);
        $this->createIndex('idx-link-view_id', Link::tableName(), ['view_id'], false);
        $this->createIndex('idx-link-viewsession_id', Link::tableName(), ['viewsession_id'], false);

        $this->addForeignKey('fk-link-user_id', Link::tableName(), 'user_id', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-link-view_id', Link::tableName(), 'view_id', View::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-link-viewsession_id', Link::tableName(), 'viewsession_id', Viewsession::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->createTable(Linksummary::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'period' => $this->integer(10)->unsigned()->notNull(),
            'period_start_at' => $this->datetime()->unsigned()->notNull(),
            'period_end_at' => $this->datetime()->unsigned()->notNull(),
            'url' => $this->string(5000)->null()->defaultValue(null),
            'referrer' => $this->string(5000)->null()->defaultValue(null),
            'user_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'usergroup' => $this->string(255)->null()->defaultValue(null)
        ], $tableOptions);

        $this->createIndex('idx-linksummary-url', Linksummary::tableName(), ['url'], false);
        $this->createIndex('idx-linksummary-referrer', Linksummary::tableName(), ['referrer'], false);
        $this->createIndex('idx-linksummary-user_id', Linksummary::tableName(), ['user_id'], false);

        $this->addForeignKey('fk-linksummary-user_id', Linksummary::tableName(), 'user_id', User::tableName(), 'id', 'SET NULL', 'CASCADE');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $this->dropForeignKey('fk-link-user_id', Link::tableName());
        $this->dropForeignKey('fk-link-view_id', Link::tableName());
        $this->dropForeignKey('fk-link-viewsession_id', Link::tableName());
        $this->dropForeignKey('fk-linksummary-user_id', Linksummary::tableName());

        $this->dropTable(Link::tableName());
        $this->dropTable(Linksummary::tableName());

        return true;
    }
}
