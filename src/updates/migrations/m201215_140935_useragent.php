<?php
/**
 * @author Christoph Möke <christophmoeke@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\updates\migrations;

use fafcms\fafcms\models\User;
use fafcms\stats\models\Link;
use fafcms\stats\models\Linksummary;
use fafcms\stats\models\Useragent;
use fafcms\stats\models\View;
use fafcms\stats\models\Viewsession;
use fafcms\updater\base\Migration;

/**
 * Class m201215_140935_useragent
 *
 * @package fafcms\stats\updates\migrations
 */
class m201215_140935_useragent extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $this->alterColumn(Useragent::tableName(), 'agent', $this->string(5000)->null()->defaultValue(null));
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $this->alterColumn(Useragent::tableName(), 'agent', $this->string(255)->null()->defaultValue(null));
        return true;
    }
}
