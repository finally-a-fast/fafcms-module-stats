<?php
/**
 * @author Christoph Möke <christophmoeke@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\updates\migrations;

use fafcms\stats\models\Useragent;
use fafcms\updater\base\Migration;

/**
 * Class m210406_125017_index
 *
 * @package fafcms\stats\updates\migrations
 */
class m210406_125017_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $this->createIndex('idx-useragent-agent', Useragent::tableName(), ['agent'], false);
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $this->dropIndex('idx-useragent-agent', Useragent::tableName());
        return true;
    }
}
