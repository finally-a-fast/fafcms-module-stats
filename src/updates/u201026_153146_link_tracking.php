<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\updates;

use fafcms\stats\updates\migrations\m201026_153146_link_tracking;
use fafcms\updater\base\Update;

/**
 * Class u201026_153146_link_tracking
 *
 * @package fafcms\stats\updates
 */
class u201026_153146_link_tracking extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        if (!$this->migrateUp(m201026_153146_link_tracking::class)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        if (!$this->migrateDown(m201026_153146_link_tracking::class)) {
            return false;
        }

        return true;
    }
}
