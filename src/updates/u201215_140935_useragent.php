<?php
/**
 * @author Christoph Möke <christophmoeke@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\updates;

use fafcms\stats\updates\migrations\m201215_140935_useragent;
use fafcms\updater\base\Update;

/**
 * Class u201215_140935_useragent
 *
 * @package fafcms\stats\updates
 */
class u201215_140935_useragent extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        if (!$this->migrateUp(m201215_140935_useragent::class)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        if (!$this->migrateDown(m201215_140935_useragent::class)) {
            return false;
        }

        return true;
    }
}
