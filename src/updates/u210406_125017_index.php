<?php
/**
 * @author Christoph Möke <christophmoeke@gmail.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-stats/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-stats
 * @see https://www.finally-a-fast.com/packages/fafcms-module-stats/docs Documentation of fafcms-module-stats
 * @since File available since Release 1.0.0
 */

namespace fafcms\stats\updates;

use fafcms\stats\updates\migrations\m210406_125017_index;
use fafcms\updater\base\Update;

/**
 * Class u210406_125017_index
 *
 * @package fafcms\stats\updates
 */
class u210406_125017_index extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        if (!$this->migrateUp(m210406_125017_index::class)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        if (!$this->migrateDown(m210406_125017_index::class)) {
            return false;
        }

        return true;
    }
}
